import Koa from 'koa'
import bodyParser from 'koa-bodyparser'
import Router from 'koa-router'
import mongoose from 'mongoose'
import mongooseVersion from 'mongoose-version'
import mongooseDelete from 'mongoose-delete'
import mongooseI18n from 'mongoose-i18n'
import defaultValue from 'mongoose-default-values'
import mongooseHidden from 'mongoose-hidden'
import logger from 'koa-logger'
import { Resource } from '../../lib'

mongoose.Promise = global.Promise
mongoose.set('debug', true)

// Handle connection to mongodb and data modeling.
mongoose.connect('mongodb://localhost:27017/koa-mongoose-rest')

const BookSchema = new mongoose.Schema({
  title: {
    type: String,
    unique: true,
    index: true
  },
  author: String,
  pubYear: String,
  content: {
    type: String,
    i18n: true
  },
  tags: [{
    type: String,
    i18n: true
  }],
  follow: [String],
  description: {
    short: {
      type: String,
      i18n: true
    },
    long: [{
      type: String,
      i18n: true
    }]
  }
}, {
  timestamps: true,
  minimize: false
})

BookSchema.plugin(defaultValue)
BookSchema.plugin(mongooseDelete, {
  overrideMethods: true
})
BookSchema.plugin(mongooseI18n, {
  locales: ['ar', 'en', 'es', 'fr', 'jp', 'ru', 'zh']
})
BookSchema.plugin(mongooseVersion, {
  collection: 'Book__versions'
})

// By default in Mongoose virtuals will not be included. Turn on before enabling mongooseHidden plugin.
BookSchema.set('toJSON', { virtuals: true })
BookSchema.set('toObject', { virtuals: true })

BookSchema.plugin(mongooseHidden())

const Book = mongoose.model('Book', BookSchema)

// Setup http server with koa2.
const app = new Koa()
const router = new Router()
app.keys = ['query parser', 'simple']
app.use(logger())
app.use(bodyParser())

// Setup koaMongooseRest endpoints.
const resource = new Resource(Book, {
  modelIdField: 'title',
  hasI18n: true,
  log: console.error
})
router.post('/books', resource.createDoc())
router.get('/books', resource.readDocs())
router.get('/books/count', resource.countDocs())
router.patch('/books', resource.patchDocs())
router.delete('/books', resource.removeDocs())
router.get('/books/:bookId', resource.readDoc())
router.patch('/books/:bookId', resource.patchDoc())
router.put('/books/:bookId', resource.putDoc())
router.delete('/books/:bookId', resource.removeDoc())

app
  .use(router.routes())
  .use(router.allowedMethods())

// Start the http server on http://localhost:3000/
app.listen(3000)
