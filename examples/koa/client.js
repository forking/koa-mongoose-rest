import axios from 'axios'

const log = console.log
let baseUrl = 'http://localhost:3000'
let testNow = ''

function run (method = 'get', apiUrl = '', mode = '', params = {}, callback = '') {
  if (testNow.length > 0 && testNow !== mode) {
    return new Promise(solve => solve())
  }
  log(`=========== ${mode} ==========`)
  if (method == 'get') {
    params = { params }
  }
  let url = `${baseUrl}${apiUrl}`
  log(method, url)
  return axios
    [method](url, params)
    .then(res => {
      log('OK:', res.data)
      if (callback !== '') {
        return callback(res.data)
      }
    })
    .catch(err => {
      log('NO:', err.response.data)
    })
}

// testNow = 'readDoc'

let docOne = {
  title: 'one',
  author: 'Jack One',
  content: {
    zh: 'ZH',
    en: 'EN',
    fr: 'FR'
  },
  // content: 'TEST...',
  pubYear: '2001',
  tags: {
    zh: ['zh-tag', 'zh-tag2'],
    en: ['EN-tag', 'EN-tag2'],
    fr: ['FR-tag', 'FR-tag2']
  },
  follow: ['f1', 'f2'],
  description: {
    short: {
      zh: 'ZH-short',
      en: 'EN-short',
      fr: 'FR-short'
    },
    long: {
      zh: ['ZH-long1', 'ZH-long2'],
      en: ['EN-long1', 'EN-long2'],
      fr: ['FR-long1', 'FR-long2']
    }
  }
}

let docTwo = {
  title: 'two',
  author: 'Smith Two',
  content: {
    zh: 'ZH',
    en: 'EN',
    fr: 'FR'
  },
  // content: 'TEST...',
  pubYear: '2005',
  tags: {
    zh: ['zh-tag', 'zh-tag2'],
    en: ['EN-tag', 'EN-tag2'],
    fr: ['FR-tag', 'FR-tag2']
  },
  follow: ['f1', 'f2'],
  description: {
    short: {
      zh: 'ZH-short',
      en: 'EN-short',
      fr: 'FR-short'
    },
    long: {
      zh: ['ZH-long1', 'ZH-long2'],
      en: ['EN-long1', 'EN-long2'],
      fr: ['FR-long1', 'FR-long2']
    }
  }
}

run('post', '/books', 'createDoc', docOne).then(() => {
  return run('post', '/books', 'createDoc', docTwo)
}).then(() => {
  return run('get', '/books', 'readDocs', {
    lang: 'zh'
  }, res => {
    res.data.forEach(item => {
      log(item.title, item.content)
    })
  })
}).then(() => {
  return run('get', '/books/count', 'countDocs')
}).then(() => {
  return run('patch', '/books/one', 'readDoc', {
    author: 'Jack One - zh'
  })
}).then(() => {
  return run('patch', '/books/two', 'readDoc', {
    author: 'Smith Two - fr'
  })
}).then(() => {
  return run('get', '/books/one', 'readDoc', {
    lang: 'zh'
  })
}).then(() => {
  return run('get', '/books/two', 'readDoc', {
    lang: 'fr'
  })
// }).then(() => {
//   return run('delete', '/books/one', 'removeDoc')
// }).then(() => {
//   return run('delete', '/books/two', 'removeDoc')
})
