/**
 * Base class for all Resources.
 *
 * @example How to quickly generate rest endpoint for your mongoose model.
 *   // ...
 *   var bookResource = new Resource(BookModel);
 *   // ...
 *   router.get('/books', bookResource.readDocs())
 *   router.post('/books', bookResource.createDoc())
 *   router.delete('/books', bookResource.removeDocs())
 *   router.get('/books/:bookId', bookResource.readDoc())
 *   router.patch('/books/:bookId', bookResource.patchDoc())
 *   router.put('/books/:bookId', bookResource.putDoc())
 *   router.delete('/books/:bookId', bookResource.removeDoc())
 *   // ...
 */
const compose = require('koa-compose')

class Resource {

  /*
   * Builds a new Resource instance given a mongoose.Model class.
   *
   * @param Model {Object} instance of mongoose.Model
   * @param {Object} options
   * @option options {String} modelName useful for composing the modelKey
   * @option options {String} modelKey useful for identifying model id in routes
   * @option options {Number} defaultPageSize pagination default page size
   * @option options {String} namespace placeholder for custom data and methods.
   * @option options {Function} log is a function(level, message, args...) {}
  */
  constructor (Model, options = {}) {
    if (!this.isMongooseModel(Model)) {
      throw new Error('Resource expected an instance of mongoose.Model')
    }

    // @property {Object} Model instance of mongoose.Model
    this.Model = Model

    // @property {String} modelName override the name of the model
    this.modelName = options.modelName || this.Model.modelName.toLowerCase()

    // @property {String} modelIdField override :modelId field name.
    this.modelIdField = options.modelIdField || '_id'
    // hasI18n=true => use mongoose-i18n-localize => extract ?lang=en filter
    this.hasI18n = options.hasI18n || false
    this.defaultLang = options.defaultLang || 'en'

    // @property {String} modelKey override :modelId key in routes.
    this.modelKey = options.modelKey || `${this.modelName}Id`

    // @property {Array<String>} reservedQueryParams ignored by filters.
    this.reservedQueryParams = ['_fields', '_skip', '_limit', '_sort']

    // @property {Array<String>} supportedQueryOperators query operators.
    this.supportedQueryOperators = ['eq', 'ne', 'lt', 'gt', 'lte',
      'gte', 'regex', 'in']

    // @property {Number} defaultPageSize default page size when paginating.
    this.defaultPageSize = options.defaultPageSize || 10

    // @property {String} ns custom mortimer data attached to http.Request.
    this.ns = options.namespace || 'kmr'

    // @property {Function} a function to log messages. Defaults to noop.
    if (this.is('Function', options.log)) {
      this.log = options.log
    } else {
      this.log = () => {} // noop.
    }
  }

  // GENERIC ENDPOINTS

  /**
   * Middleware stack which creates a new document.
   *
   * @example Bind this middleware to an express endpoint.
   *   var bookResource = new Resource(BookModel);
   *   // ...
   *   router.post('/books', bookResource.createDoc());
   *
   * @return {Array<Function>} list of express compatible middleware functions.
   */
  createDoc () {
    return compose([
      this.namespace(),
      this.create(),
      this.publish({statusCode: 201})
    ])
  }

  /**
   * Middleware stack which reads a document by it's id.
   *
   * @example Bind this middleware to an express endpoint.
   *   var bookResource = new Resource(BookModel);
   *   // ...
   *   router.get('/books/:bookId', bookResource.readDoc());
   *
   * @return {Array<Function>} list of express compatible middleware functions.
   */
  readDoc () {
    return compose([
      this.namespace(),
      this.read(),
      this.filters(),
      this.fields(),
      this.execute(),
      this.publish({statusCode: 200})
    ])
  }

  /**
   * Middleware stack which updates a document by the provided id.
   *
   * @example Bind this middleware to an express endpoint.
   *   var bookResource = new Resource(BookModel);
   *   // ...
   *   router.patch('/books/:bookId', bookResource.patchDoc());
   *
   * @return {Array<Function>} list of express compatible middleware functions.
   */
  patchDoc () {
    return compose([
      this.namespace(),
      this.read(),
      this.execute(),
      this.patch(),
      this.publish({statusCode: 200})
    ])
  }

  /**
   * Middleware stack which replaces existing resource with the provided
   * request body.
   *
   * @example Bind this middleware to an express endpoint.
   *   var bookResource = new Resource(BookModel);
   *   // ...
   *   router.put('/books/:bookId', bookResource.putDoc());
   */
  putDoc () {
    return compose([
      this.namespace(),
      this.read(),
      this.execute(),
      this.put(),
      this.publish({statusCode: 200})
    ])
  }

  /**
   * Middleware stack which removes a document specified by id.
   *
   * @example Bind this middleware to an express endpoint.
   *   var bookResource = new Resource(BookModel);
   *   // ...
   *   router.delete('/books/:bookId', bookResource.removeDoc());
   *
   * @return {Array<Function>} list of express compatible middleware functions.
   */
  removeDoc () {
    return compose([
      this.namespace(),
      this.read(),
      this.execute(),
      this.remove(),
      this.publish({statusCode: 204, empty: true})
    ])
  }

  /**
   * Middleware stack which returns a list of documents from the database.
   * It supports advanced filtering, pagination, sorting, field selection, etc.
   *
   * @example Bind this middleware to an express endpoint.
   *   var bookResource = new Resource(BookModel);
   *   // ...
   *   router.get('/books', bookResource.readDocs());
   *
   * @return {Array<Function>} list of express compatible middleware functions.
   */
  readDocs () {
    return compose([
      this.namespace(),
      this.readAll(),
      this.pagination(),
      this.filters(),
      this.sort(),
      this.fields(),
      this.execute(),
      this.publish({statusCode: 200})
    ])
  }

  /**
   * Middleware stack which patches all documents selected by the query params.
   * This endpoint does not return the modified documents.
   * For that you will need to perform another request.
   *
   * @example Bind this middleware to an express endpoint.
   *   var bookResource = new Resource(BookModel);
   *   // ...
   *   router.patch('/books', bookResource.patchDocs());
   *
   * @return {Array<Function>} list of express compatible middleware functions.
   */
  patchDocs () {
    return compose([
      this.namespace(),
      this.readAll(),
      this.pagination(),
      this.filters(),
      this.sort(),
      this.patchAll(),
      this.publish({statusCode: 200})
    ])
  }

  /**
   * Middleware stack which removes all documents matched by the query filters.
   *
   * @example Bind this middleware to an express endpoint.
   *   var bookResource = new Resource(BookModel);
   *   // ...
   *   router.delete('/books', bookResource.removeDocs());
   *
   * @return {Array<Function>} list of express compatible middleware functions.
   */
  removeDocs () {
    return compose([
      this.namespace(),
      this.readAll(),
      this.pagination(),
      this.filters(),
      this.sort(),
      this.removeAll(),
      this.publish({statusCode: 200, empty: true})
    ])
  }

  /**
   * Middleware stack which returns the number of documents in a collection.
   * Supports filters.
   *
   * This endpoint is separate from readDocs because of performance issues,
   * mongo does not return a count of all documents when using skip, limit.
   *
   * @example Bind this middleware to an express endpoint.
   *   var bookResource = new Resource(BookModel);
   *   // ...
   *   router.get('/books/count', bookResource.countDocs());
   *
   * @return {Array<Function>} list of express compatible middleware functions.
   */
  countDocs () {
    return compose([
      this.namespace(),
      this.readAll(),
      this.filters(),
      this.countAll(),
      this.publish({statusCode: 200})
    ])
  }

  // MIDDLEWARE

  /**
   * Returns a middleware which sets a namespace object on the http.Request
   * instance built by express. It is used to attach data and custom functions.
   *
   * @return {Function} express compatible middleware function.
   */
  namespace () {
    return (ctx, next) => {
      ctx.state[this.ns] = {}
      if (this.hasI18n) {
        ctx.state[this.ns].lang = this.defaultLang
      }
      return next()
    }
  }

  /**
   * Middleware start the mongoose.Query object for fetching a
   * model from the database.
   *
   * @param {String} req.params.<modelName>Id id of model to be returned
   * @param {mongoose.Query} ctx.request.<ns>.query the mongoose.Query instance
   *                                    that will fetch the data from Mongo.
   * @return {Function} middleware function
   */
  read () {
    return (ctx, next) => {
      const id = ctx.params[this.modelKey]
      if (!id) {
        ctx.status = 404
        ctx.body = {
          msg: 'Document id not provided'
        }
        return
      }

      ctx.state[this.ns].query = this.Model.findOne().where(this.modelIdField).equals(id)
      return next()
    }
  }

  /**
   * Middleware creates a document of the current model type
   * from the request json payload. It also publishes the
   * newly created document.
   *
   * Note that this middleware creates the new document from the received
   * body without performing validation, this is left to the implemention.
   *
   * @param {Object} ctx.request.body the request payload object to be wrapped.
   * @param {mongose.Model} ctx.request.<ns>.result newly created instance of model.
   * @return {Function} middleware function
   */
  create () {
    return (ctx, next) => {
      const document = new this.Model(ctx.request.body)
      return document.save()
        .then(document => {
          ctx.state[this.ns].result = document
          return next()
        })
        .catch(error => {
          this.log('error', 'Failed to store document', error)
          ctx.status = 500
          ctx.body = {
            msg: 'Error storing new document'
          }
        })
    }
  }

  /**
   * Middleware updates one record in the database by it's id.
   * If a record isn't found an error is thrown.
   *
   * @param {Object} ctx.request.body the request payload to be added over the
   *                          existing model record.
   * @param {mongoose.Model} ctx.request.<ns>.result instance of the Mode to be updated.
   * @param {Mixed} ctx.request.<ns>.result instance of the Model that was just updated.
   * @return {Function} middleware function
   */
  patch () {
    return (ctx, next) => {
      Object.keys(ctx.request.body).forEach((path) => {
        const newValue = ctx.request.body[path]
        ctx.state[this.ns].result.set(path, newValue)
      })

      return ctx.state[this.ns].result.save()
        .then(res => next())
        .catch(error => {
          this.log('error', 'Failed to patch document', error)
          ctx.status = 500
          ctx.body = {
            msg: 'Error patching document'
          }
        })
    }
  }

  /**
   * Middleware replaces a document with the provided body.
   *
   * Because no better way is available in mongoose, this middleware will
   * remove the existing document then insert it again with the new data.
   * Please note that this endpoint is not thread safe!
   *
   * @param {Object} ctx.request.<ns>.result instance of current Model to be replaced.
   * @return {Function} middleware function
   */
  put () {
    return (ctx, next) => {
      return ctx.state[this.ns].result.remove()
        .then(() => {
          const document = new this.Model(ctx.request.body)
          document._id = ctx.state[this.ns].result._id
          return document.save()
            .then(document => {
              ctx.state[this.ns].result = document
              return next()
            })
            .catch(error => {
              this.log('error', 'Failed to update document', error)
              ctx.status = 500
              ctx.body = {
                msg: 'Error replacing document'
              }
            })
        })
        .catch(error => {
          this.log('error', 'Failed to read document for update', error)
          ctx.status = 500
          ctx.body = {
            msg: 'Failed to replace the document'
          }
        })
    }
  }

  /**
   * Middleware removes the specified document from the db if it
   * belongs to the current shop.
   *
   * @param {String} req.params.modelId id of model to be updated
   * @param {Object} ctx.request.<ns>.result instance of current Model that was removed
   * @return {Function} middleware function
   */
  remove () {
    return (ctx, next) => {
      return ctx.state[this.ns].result.remove()
        .then(() => next())
        .catch(error => {
          this.log('error', 'Failed to remove document', error)
          ctx.status = 500
          ctx.body = {
            msg: 'error removing document'
          }
        })
    }
  }
  /**
   * Middleware creates a mongoose.Query instance that fetches all
   * models from the database.
   *
   * @param {mongoose.Query} ctx.request.<ns>.query instance of mongoose.Query to
   *                                         fetch models from database.
   * @return {Function} middleware function
   */
  readAll () {
    return (ctx, next) => {
      ctx.state[this.ns].query = this.Model.find()
      return next()
    }
  }

  /**
   * Middleware adds an update clause to query being constructed then executes
   * it. This way it updates all documents selected by the query.
   *
   * @param {Object} ctx.request.body payload to overwrite data on selected documents.
   * @param {mongoose.Query} ctx.request.<ns>.query instance of mongoose.Query to
   *                                        fetch models from database.
   * @return {Function} middleware function
   */
  patchAll () {
    return (ctx, next) => {
      ctx.state[this.ns].query.setOptions({multi: true})
      return ctx.state[this.ns].query.update(ctx.request.body)
        .then(() => next())
        .catch(error => {
          this.log('error', 'Failed to update collection', error)
          ctx.status = 500
          ctx.body = {
            msg: 'Unable to patch selected documents'
          }
        })
    }
  }

  /**
   * Middleware to remove all documents selected previously.
   *
   * @param {Number|Object|Array<Object>} ctx.request.<ns>.result query result.
   * @return {Function} middleware function
   */
  removeAll () {
    return (ctx, next) => {
      return ctx.state[this.ns].query.remove()
        .then(() => next())
        .catch(error => {
          this.log('error', 'Failed to remove collection', error)
          ctx.status = 500
          ctx.body = {
            msg: 'Failed to remove selected documents'
          }
        })
    }
  }

  updateAll () {
    // TODO this middleware should update all documents that support this.
  }

  /**
   * Middleware counts the number of items currently selected.
   *
   * @param {Number} ctx.request.<ns>.result the result of the count query.
   * @return {Function} middleware function
   */
  countAll () {
    return (ctx, next) => {
      return ctx.state[this.ns].query.count()
        .then(count => {
          ctx.state[this.ns].result = count
          return next()
        })
        .catch(error => {
          this.log('error', 'Failed to count documents', error)
          ctx.status = 500
          ctx.body = {
            msg: 'Error counting documents'
          }
        })
    }
  }

  /**
   * Executes the current built query. It will set the the
   * results in ctx.request.<ns>.result or return an error message if
   * something goes wrong.
   *
   * @param {mongoose.Query} ctx.request.<ns>.query query for mongodb.
   * @param {Number|Object|Array<Object>} ctx.request.<ns>.result query result.
   * @return {Function} middleware function
   */
  execute () {
    return (ctx, next) => {
      return ctx.state[this.ns].query.exec()
        .then(documents => {
          if (!documents) {
            ctx.status = 404
            ctx.body = {
              msg: 'Resources not found'
            }
            return
          }
          ctx.state[this.ns].result = documents
          return next()
        })
        .catch(error => {
          this.log('error', 'Failed to execute mongoose query', error)
          ctx.status = 500
          ctx.body = {
            msg: error.message
          }
        })
    }
  }

  /**
   * Middleware prints the results of a previously executed Query.
   *
   * @param ctx.request.<ns>.result {Object} results from mongoose.Query
   *                                 instance, it's either a Document
   *                                 or an array of Documents.
   * @param {Object} options
   * @param options {Number} statusCode status code  returned to the client.
   * @param options {Boolean} empty - if the response payload should be empty.
   * @return {Function} middleware function
   */
  publish (options = {}) {
    return (ctx) => {
      options.statusCode = options.statusCode || 200
      ctx.status = options.statusCode

      options.empty = options.empty || false
      if (options.empty === true) return

      ctx.body = {
        meta: Object.assign({}, ctx.request.query),
        data: this.format(ctx.state[this.ns].result, ctx.state[this.ns])
      }
    }
  }

  // QUERY MODIFIERS

  /**
   * Middleware modifies the current query object to only fetch
   * specified fields
   *
   * @example Have the endpoint return only a subset of the data in docs.
   *   request.get('/books?_fields=author,title')
   *
   * @param {String} ctx.request.query._fields list of coma separated field keys
   * @param {mongoose.Query} ctx.request.<ns>.query fetches the data from Mongo.
   * @return {Function} middleware function
   */
  fields () {
    return (ctx, next) => {
      if (!ctx.request.query._fields) {
        return next()
      }

      const fields = ctx.request.query._fields.split(',').join(' ')
      ctx.state[this.ns].query.select(fields)
      return next()
    }
  }

  /**
   * Middleware applies pagination parameter to the current query.
   *
   * @example Have the endpoint return a slice of the collection
   *   request.get('/books?_skip=100&_limit=200')
   *
   * @param {Number} ctx.request.query._skip where to start fetching the result set.
   * @param {Number} ctx.request.query._limit how many records to return
   * @param {mongoose.Query} ctx.request.<ns>.query fetch models from database.
   * @return {Function} middleware function
   */
  pagination () {
    return (ctx, next) => {
      const skip = ctx.request.query._skip ? +ctx.request.query._skip : 0
      const limit = ctx.request.query._limit ? +ctx.request.query._limit : this.defaultPageSize
      ctx.state[this.ns].query.skip(skip)
      ctx.state[this.ns].query.limit(limit)
      return next()
    }
  }

  /**
   * Middleware filters the results of the current query, with
   * the params from the request url query string.
   *
   * It will ignore the reserved query params attached to the class.
   *
   * This middleware supports multiple operators matching those in mongo:
   *  eq, ne, lt, lte, gt, gte, regex, etc.
   *
   * @example How to find all books named Hamlet
   *   request.get('/books?title__eq=Hamlet')
   *
   * @example How to find all books with more than 1000 pages
   *   request.get('/books?numPages__gte=1000')
   *
   * @example How to find all books written in russian, between 1800 and 1900
   *   request.get('/books?lang=ru&writtenIn__gte=1800&writtenIn__lte=1900')
   *
   * @param {Object} ctx.request.query query string params acting as filters
   * @param {mongoose.Query} ctx.request.<ns>.query fetches models from database.
   * @return {Function} middleware function
   */
  filters () {
    return (ctx, next) => {
      Object.keys(ctx.request.query).forEach((key) => {
        if (this.reservedQueryParams.indexOf(key) !== -1) {
          return
        }

        let value = ctx.request.query[key]

        if (key === 'lang' && this.hasI18n) {
          ctx.state[this.ns].lang = value || this.defaultLang
          return
        }

        const parts = key.split('__')
        const operand = parts[0]
        let operator = parts[1] || 'eq'

        if (this.supportedQueryOperators.indexOf(operator) === -1) {
          return
        }

        switch (operator) {
          case 'gt':
            value = +value // cast to int.
            break
          case 'gte':
            value = +value // cast to int
            break
          case 'lt':
            value = +value // cast to int
            break
          case 'lte':
            value = +value // cast to int
            break
          case 'in':
            value = value.split(',') // transform into array.
            break
          case 'regex':
            value = new RegExp(value, 'gim')
            break
        }
        ctx.state[this.ns].query.where(operand)[operator](value)
      })

      return next()
    }
  }

  /**
   * Middleware sorts the result set by the given _sort field.
   *
   * The _sort value is a field name of the current model by
   * which the sort will be performed.
   *
   * In addition the field name can be prefixed with `-` to
   * indicate sorting in descending order.
   *
   * @example Retrieve all books sorted by title descending.
   *   request.get('/books?_sort=-title')
   *
   * @param {Object} ctx.request.query dict with url query string params.
   * @param {String} ctx.request.query._sort the field/order to sort by.
   *                                 Eg `&_sort=-createdOn`
   * @param {Object} ctx.request.<ns>.query instance of mongoose.Query to
   *                                fetch models from database.
   * @return {Function} middleware function
   */
  sort () {
    return (ctx, next) => {
      if (this.is('String', ctx.request.query._sort)) {
        ctx.state[this.ns].query.sort(ctx.request.query._sort)
      }
      return next()
    }
  }

  /**
   * Format the database results. By default, mongoose serializes the
   * documents, but you should override this method for custom formatting.
   *
   * @param {Object} results depend on the resolved query.
   * @param {Object} state => ctx.state[this.ns]
   *
   * @return {Function} middleware function
   */
  format (results, state) {
    if (this.hasI18n && (typeof results === 'object')) {
      return this.Model.schema.methods.toJSONLocalizedOnly(results, state.lang)
    }
    return results
  }

  /**
   * Helper function to check if a value is of a given type.
   * @param {String} type - one of Array, Function, String, Number, Boolean...
   * @param {Object} value - any javascript value.
   *
   * @return {Boolean}
   */
  is (type, value) {
    return Object.prototype.toString.call(value) === `[object ${type}]`
  }

  /**
   * Determins whether the passed in constructor function is a mongoose.Model.
   * @param {Function} model
   *
   * @return {Boolean}
   */
  isMongooseModel (model) {
    return this.is('Function', model) &&
           this.is('String', model.modelName) &&
           this.is('Function', model.findOne) &&
           this.is('Function', model.find)
  }
}

// Public API.
exports.Resource = Resource
